package com.example.dehoyos.examenu1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.example.dehoyos.examenu1.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Llamado cuando el usuario hace click en el boton Enviar */
    public void enviarMensaje(View view) {

        Intent intent = new Intent(this, DisplayMessageActivity.class);

        EditText editText = (EditText) findViewById(R.id.edit_message);
        EditText editText2 = (EditText) findViewById(R.id.edit_message2);
        String message = editText.getText().toString();

        String message2 = editText2.getText().toString();
        Integer numero = Integer.parseInt(message2);

        String charMin = "abcdefghijklmnopqrstuvwxyz";
        String charMay = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String salida = "";
        for(int i = 0;i<message.length();i++){
            if((charMin.indexOf(message.charAt(i)) != -1) || (charMay.indexOf(message.charAt(i)) != -1))
                salida += (charMin.indexOf(message.charAt(i)) != -1) ? charMin.charAt( ((charMin.indexOf(message.charAt(i)) )+numero)%charMin.length() ) : charMay.charAt(( charMay.indexOf(message.charAt(i)) +numero)%charMay.length());
            else
                salida += message.charAt(i);
        }
        String msje2 = "Codigo Cesar: " + salida;
        intent.putExtra(EXTRA_MESSAGE, msje2);
        startActivity(intent);

    }


}
